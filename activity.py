year = int(input("Please input a year: \n"))

if year%4 == 0:
	print(f"{year} is a leap year")
else:
	print(f"{year} is not a leap year")

rows = int(input("Enter number of rows: \n"))
columns = int(input("Enter number of columns: \n"))

for r in range(rows):
	for c in range(columns):
		print("*", end = "")
	print()